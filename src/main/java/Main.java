import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main (String[] args){

        TestSuit testSuit01 = new TestSuit("Test Suit 1", "The first test suit");
        TestSuit testSuit02 = new TestSuit("Test Suit 2", "The second test suit");

        TestCase testCase01 = new TestCase("Test Case 1", "First test case", new ArrayList<>(Arrays.asList("Step01", "Step02")), "Show first test case", "Open");
        TestCase testCase02 = new TestCase("Test Case 2", "Second test case", new ArrayList<>(Arrays.asList("Step01", "Step02")), "Show Second test case", "Open");
        TestCase testCase03 = new TestCase("Test Case 3", "Third test case", new ArrayList<>(Arrays.asList("Step01", "Step02")), "Show Third test case", "Open");
        TestCase testCase04 = new TestCase("Test Case 4", "Fourth test case", new ArrayList<>(Arrays.asList("Step01", "Step02")), "Show Fourth test case", "Open");
        TestCase testCase05 = new TestCase("Test Case 5", "Fifth test case", new ArrayList<>(Arrays.asList("Step01", "Step02")), "Show Fifth test case", "Open");
        TestCase testCase06 = new TestCase("Test Case 6", "Sixth test case", new ArrayList<>(Arrays.asList("Step01", "Step02")), "Show Sixth test case", "Open");
        TestCase testCase07 = new TestCase("Test Case 7", "Seventh test case", new ArrayList<>(Arrays.asList("Step01", "Step02")), "Show Seventh test case", "Open");
        TestCase testCase08 = new TestCase("Test Case 8", "Eight test case", new ArrayList<>(Arrays.asList("Step01", "Step02")), "Show Eight test case", "Open");
        TestCase testCase09 = new TestCase("Test Case 9", "Nine test case", new ArrayList<>(Arrays.asList("Step01", "Step02")), "Show Nine test case", "Open");
        TestCase testCase10 = new TestCase("Test Case 10", "Ten test case", new ArrayList<>(Arrays.asList("Step01", "Step02")), "Show Ten test case", "Open");

        Owner owner01 = new Owner("Valeria", "Lopez", "QA");
        Owner owner02 = new Owner("Jhon", "Smith", "DEV");
        Owner owner03 = new Owner("Charly", "Sheen", "Manager");


        // The testcase5 and testcase10 doesn't have an owner
        testCase01.setOwner(owner01);
        testCase02.setOwner(owner02);
        testCase03.setOwner(owner03);
        testCase04.setOwner(owner02);
        testCase06.setOwner(owner01);
        testCase07.setOwner(owner01);
        testCase08.setOwner(owner02);
        testCase09.setOwner(owner01);


        testSuit01.setTestCases(new ArrayList<>(Arrays.asList(testCase01, testCase02, testCase03, testCase04, testCase05)));
        testSuit02.setTestCases(new ArrayList<>(Arrays.asList(testCase06, testCase07, testCase08, testCase09, testCase10)));

        // Test Suit 1 Info
        System.out.println(testSuit01.getName());
        for (TestCase testCase : testSuit01.getTestCases()) {
            printTestcase(testCase);
        }


        // Test Suit 2 Info
        System.out.println(testSuit02.getName());
        for (TestCase testCase : testSuit02.getTestCases()) {
            printTestcase(testCase);
        }


        ArrayList<TestCase> testCasesForOwner1 = testSuit01.getTestCasesByOwner(owner01);
        testCasesForOwner1.addAll(testSuit02.getTestCasesByOwner(owner01));
        System.out.println("Test cases for: " + owner01.getFullName());
        for (TestCase testcase: testCasesForOwner1) {
            printTestcase(testcase);
        }

        testCase01.setPriority(2);
        testCase04.setPriority(2);
        testCase07.setPriority(2);
        testCase08.setPriority(2);

        ArrayList<TestCase> testCasesForPriority1 = testSuit01.getTestCasesByPriority(2);
        testCasesForPriority1.addAll(testSuit02.getTestCasesByPriority(2));
        System.out.println("Test cases with priority: " + 2);
        for (TestCase testcase: testCasesForPriority1){
            printTestcase(testcase);
        }

    }

    private static void printTestcase(TestCase testCase) {
        System.out.println("   - " + testCase.getTitle() + ": " + testCase.getDescription());
        System.out.println("     Priority: " + testCase.getPriority());
        String ownerFullName =  testCase.getOwner() == null ? "Unassigned" : testCase.getOwner().getFullName();
        System.out.println("     Owner: " + ownerFullName);

        System.out.print("     Steps:");
        for (String step: testCase.getSteps()) {
            System.out.print(" " + step + ",");
        }
        System.out.println();

        System.out.println("     Expected Result: " + testCase.getExpectedResult());
        System.out.println("     Status: " + testCase.getStatus());

        System.out.println();
    }
}
