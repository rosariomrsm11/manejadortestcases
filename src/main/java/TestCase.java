import java.util.ArrayList;

public class TestCase {

    private String title;
    private int priority;
    private Owner owner;
    private String description;
    private ArrayList<String> steps;
    private String expectedResult;
    private String status;


    public TestCase(String title, String description, ArrayList<String> steps, String expectedResult, String status) {
        this.title = title;
        this.priority = 1;
        this.description = description;
        this.steps = steps;
        this.expectedResult = expectedResult;
        this.status = status;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getTitle() {
        return title;
    }

    public int getPriority() {
        return priority;
    }

    public Owner getOwner() {
        return owner;
    }

    public String getDescription() {
        return description;
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    public String getStatus() {
        return status;
    }

    public ArrayList<String> getSteps() {
        return steps;
    }

    @Override
    public String toString() {
        return "   - " + this.title + " " + this.description + "\n";
    }
}
