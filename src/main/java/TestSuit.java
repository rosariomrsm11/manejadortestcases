import java.lang.reflect.Array;
import java.util.ArrayList;

public class TestSuit {

    private String name;
    private String description;
    private ArrayList<TestCase> testCases;

    public TestSuit(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setTestCases(ArrayList<TestCase> testCases) {
        this.testCases = testCases;
    }


    public ArrayList<TestCase> getTestCases() {
        return testCases;
    }

    public ArrayList<TestCase> getTestCasesByOwner(Owner owner) {
        ArrayList<TestCase> testCases = new ArrayList<>();
        // iterar todos los test cases
        for (TestCase testcase: this.testCases) {
            if (testcase.getOwner() == null) {
                continue;
            }
            if (testcase.getOwner().getName().equals(owner.getName()) && testcase.getOwner().getLastName().equals(owner.getLastName())) {
                testCases.add(testcase);
            }
        }

        return testCases;
    }

    public ArrayList<TestCase> getTestCasesByPriority(int priority) {
        ArrayList<TestCase> testCases = new ArrayList<>();
        int count = 0;

        while (count < this.testCases.size()){
            TestCase testcase = this.testCases.get(count);

            if (testcase.getPriority() == priority){
                testCases.add(testcase);
            }
            count++;
        }

        return testCases;
    }


}
