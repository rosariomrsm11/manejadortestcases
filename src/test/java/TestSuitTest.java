import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class TestSuitTest {

    @Test
    public void VerifyFilterWithNullOwner() {
        TestSuit testSuit01 = new TestSuit("Test Suit 1", "The first test suit");
        TestCase testCase01 = new TestCase("Test Case 1", "First test case", new ArrayList<>(Arrays.asList("Step01", "Step02")), "Show first test case", "Open");
        testSuit01.setTestCases(new ArrayList<>(Arrays.asList(testCase01)));

        ArrayList<TestCase> testCasesForOwner1 = testSuit01.getTestCasesByOwner(null);

        Assert.assertEquals(0, testCasesForOwner1.size());
    }


    @Test
    public void VerifyFilterWithInvalidOwner() {
        TestSuit testSuit01 = new TestSuit("Test Suit 1", "The first test suit");
        TestCase testCase01 = new TestCase("Test Case 1", "First test case", new ArrayList<>(Arrays.asList("Step01", "Step02")), "Show first test case", "Open");
        testSuit01.setTestCases(new ArrayList<>(Arrays.asList(testCase01)));
        Owner owner01 = new Owner("Valeria", "Lopez", "QA");

        ArrayList<TestCase> testCasesForOwner1 = testSuit01.getTestCasesByOwner(owner01);

        Assert.assertEquals(0, testCasesForOwner1.size());
    }

    @Test
    public void VerifyFilterByPriority() {
        TestSuit testSuit01 = new TestSuit("Test Suit 1", "The first test suit");
        TestCase testCase01 = new TestCase("Test Case 1", "First test case", new ArrayList<>(Arrays.asList("Step01", "Step02")), "Show first test case", "Open");
        testSuit01.setTestCases(new ArrayList<>(Arrays.asList(testCase01)));
        testCase01.setPriority(2);

        ArrayList<TestCase> testCasesForOwner1 = testSuit01.getTestCasesByPriority(2);

        Assert.assertEquals(1, testCasesForOwner1.size());
    }

}
